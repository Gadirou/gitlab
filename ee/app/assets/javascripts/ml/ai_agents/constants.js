import { s__ } from '~/locale';

export const ROUTE_LIST_AGENTS = 'list';
export const ROUTE_NEW_AGENT = 'create';
export const ROUTE_SHOW_AGENT = 'show';
export const ROUTE_EDIT_AGENT = 'edit';

export const I18N_AGENT_NAME_LABEL = s__('AIAgent|Agent name');
export const I18N_PROMPT_LABEL = s__('AIAgent|Prompt (optional)');
export const I18N_CREATE_AGENT = s__('AIAgent|Create agent');
export const I18N_UPDATE_AGENT = s__('AIAgent|Update agent');
export const I18N_EDIT_AGENT = s__('AIAgent|Edit Ai Agent');

export const I18N_DEFAULT_NOT_FOUND_ERROR = s__('AIAgents|The requested agent was not found.');
export const I18N_DEFAULT_SAVE_ERROR = s__('AIAgents|An error has occurred when saving the agent.');
